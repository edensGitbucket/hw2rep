import pandas


class Data:
    def __init__(self, path):
        df = pandas.read_csv(path)
        self.data = df.to_dict(orient="list")

    def __len__(self):
        return len(self.data)

    def __str__(self):
        return '{self.data}'.format(self=self)

    def select_features(self, features):
        """remove unneeded features from the data"""
        needed_features = features.split(', ')
        for feature in list(self.data):
            if feature not in needed_features:
                self.data.pop(feature)
