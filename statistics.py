def mean(values):
    """returns the mean of the values in list"""
    return sum(values) / len(values)


def median(values):
    """returns the median of the values in list"""
    sorted_list = sorted(values)
    center_index = int(len(values) / 2)  # round to int required because division always produces float

    # Median value depends on length on list
    if len(values) % 2 == 0:
        result = (sorted_list[center_index] + sorted_list[center_index - 1]) / 2
    else:
        # Now we need only 1 index for exact value
        result = sorted_list[center_index]
    return result
