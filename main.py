import sys
from flights import Flights
from data import Data
from statistics import mean, median


def main(argv):
    input_data = Data(argv[1])
    input_data.select_features(argv[2])
    flights = Flights(input_data.data)

    print("Question 1:")
    city_letters = ['D', 'A', 'T', 'S', 'C', 'I', 'E', 'N']
    flights.filter_by_airport_names("Origin_airport", city_letters)
    features = ["Distance", "Flights", "Passengers", "Seats"]
    statistic_functions = [mean, median]
    flights.print_details(features, statistic_functions)
    print()

    print("Question 2:")
    flights.compute_empty_seats()
    bad_flights = flights.count_bad_flights(3000)
    print("Number of the unwanted flights: {}".format(bad_flights))
    separated = "No"
    if bad_flights > 3120:
        separated = "Yes"
    print("Will Mr & Mrs Smith be separated? {}".format(separated))


if __name__ == "__main__":
    main(sys.argv)
