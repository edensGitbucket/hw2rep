from statistics import mean


class Flights:
    def __init__(self, data):
        self.data = data

    def filter_by_airport_names(self, airports, letters):
        item_removed = 0  # keep record of number of items removed from dictionary
        i = 0  # index of current item
        while i - item_removed < len(self.data[airports]):
            found = False
            for letter in letters:
                if letter == self.data[airports][i - item_removed][0]:
                    found = True
            if not found:
                for key in self.data:
                    self.data[key].pop(i - item_removed)
                item_removed += 1
            i += 1

    def print_details(self, features, statistic_functions):
        for key in features:
            print(key.title() + ': ', end='')
            for i in range(0, len(statistic_functions)):
                if i < len(statistic_functions) - 1:
                    print(statistic_functions[i](self.data[key]), end=', ')
                else:
                    print(statistic_functions[i](self.data[key]), end='')
            print()

    def compute_empty_seats(self):
        self.data["Empty_seats"] = []
        for i in range(len(self.data["Seats"])):
            self.data["Empty_seats"].append(self.data["Seats"][i] - self.data["Passengers"][i])

    def count_bad_flights(self, num):
        counter = 0
        empty_seats_average = mean(self.data["Empty_seats"])
        for seats in self.data["Empty_seats"]:
            if abs(seats - empty_seats_average) >= num:
                counter += 1
        return counter

    def __str__(self):
        return '{self.data}'.format(self=self)
